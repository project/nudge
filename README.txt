
Copyright 2007 http://2bits.com

Description
-----------
Allows users to nudge other users about posts they have written in the past.

Installation
------------
To install, copy the nudge directory and all its contents to your modules
directory.

Configuration
-------------
To enable this module, visit Administer -> Site building -> Modules.

To configure it, go to Administer -> Site configuration -> Nudge.

Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/nudge

Author
------
Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.
